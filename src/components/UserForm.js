import { useEffect, useState } from "react";

function UserForm () {
    const [firstname,setfirstname] = useState(localStorage.getItem("firstName"));
    const [lastname,setlastname] = useState(localStorage.getItem("lastName"));

    const handlefirstNameCHange = (event) => {
        console.log(event.target.value);
        setfirstname(event.target.value);
    }

    const handlelastNameCHange = (event) => {
        console.log(event.target.value);
        setlastname(event.target.value);
    }
    useEffect(() => {
        localStorage.setItem("firstName",firstname);
        localStorage.setItem("lastName",lastname);
    })
    return (
        <>
            <input value={firstname} onChange={handlefirstNameCHange} placeholder="Devcamp"></input><br/>
            <input value={lastname} onChange={handlelastNameCHange} placeholder="user" ></input><br/>
            <p>{firstname} {lastname}</p>
        </>
    )
}

export default UserForm;
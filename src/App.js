import logo from './logo.svg';
import './App.css';
import UserForm from './components/UserForm';

function App() {
  return (
    <div style={{textAlign:"center"}} >
      <UserForm/>
    </div>
  );
}

export default App;
